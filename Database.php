<?php

namespace FpDbTest;

use http\Exception\RuntimeException;
use mysqli;

class Database implements DatabaseInterface
{
    private mysqli $mysqli;
    private $excludeBlockParams = [
        5000
    ];

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    public function skip()
    {
        return 5000;
    }

    public function buildQuery(string $query, array $args = []): string
    {
        // разбиваем запрос на части
        $parts = $this->splitQueryOnParts($query);
        // строим запрос из частей
        return $this->buildQueryFromParts($parts, $args);
    }

    function splitQueryOnParts($query)
    {
        $parts = [];
        $part = '';
        for ($i = 0; $i < strlen($query); $i++) {
            $char = $query[$i];
            switch ($char) {
                case '?' :
                    $parts[] = new Part(PartType::TEXT, $part);
                    $part = '';
                    $i = $this->parseOperator($query, $i, $parts);
                    break;
                case '{' :
                    $parts[] = new Part(PartType::TEXT, $part);
                    $part = '';
                    $i = $this->parseBlock($query, $i, $parts);
                    break;
                default :
                    $part .= $char;
                    break;
            }
        }
        if (!empty($part)) {
            $parts[] = new Part(PartType::TEXT, $part);
        }
        return $parts;
    }

    /**
     * @param string $query
     * @param int $index
     * @param Part[] $parts
     * @return int
     */
    function parseOperator(string $query, int $index, array &$parts): int
    {
        $operator = $query[$index];
        // если есть следующий символ, то добавляем его к оператору это будет тип преобразования
        $next = $index + 1;
        if (isset($query[$next]) && $query[$next] != ' ') {
            $operator .= $query[$next];
            $index++;
        }
        $parts[] = new Part(PartType::OPERATOR, $operator);
        // возвращаем позицию на которой закончили
        return $index;
    }

    /**
     * @param string $query
     * @param int $index
     * @param Part[] $parts
     * @return int
     */
    function parseBlock(string $query, int $index, array &$parts)
    {
        $countOpen = 1;
        $subStr = '';
        for ($iBrace = $index + 1; $iBrace < strlen($query); $iBrace++) {
            $charBraces = $query[$iBrace];
            if ($charBraces === '{') {
                $countOpen++;
            }
            if ($charBraces === '}') {
                $countOpen--;
            }
            if ($countOpen > 1) {
                throw new RuntimeException("Nested blocks is unsupported");
            }
            if ($countOpen == 0) {
                $parts[] = new Part(PartType::BLOCK, $this->splitQueryOnParts($subStr));
                // возвращаем позицию на которой закончили
                return $iBrace;
            }
            $subStr .= $charBraces;
        }
        throw new RuntimeException("Failed parse block, position: " . $index);
    }

    /**
     * @param Part[] $parts
     * @param array $params
     * @param $indexParam
     * @param $excludeParams
     * @return string
     */
    function buildQueryFromParts(array $parts, array $params, &$indexParam = 0, $excludeParams = [])
    {
        $query = '';
        foreach ($parts as $part) {
            switch ($part->type) {
                case PartType::TEXT :
                    $query .= $part->value;
                    break;
                case PartType::OPERATOR :
                    $curIndex = $indexParam++;
                    if (!isset($params[$curIndex])) {
                        throw new RuntimeException("Missing parameters, current index: " . $curIndex);
                    }
                    $param = $params[$curIndex];
                    $castedValue = $this->castParam($param, $part->value);
                    // проверяем запрещённые значения
                    if (in_array($castedValue, $excludeParams)) {
                        return '';
                    }
                    $query .= $castedValue;
                    break;
                case PartType::BLOCK :
                    // обрабатываем запрос внутри блока, передаём список запрещённых параметров внутрь на проверку
                    $query .= $this->buildQueryFromParts($part->value, $params, $indexParam, $this->excludeBlockParams);
                    break;
                default :
                    throw new RuntimeException('Unknown PartType: ' . print_r($part->type, true));
            }
        }
        return $query;
    }

    function castParam($value, $operator)
    {
        switch ($operator) {
            case '?' :
                return $this->escapeSimple($value);
            case '?d' :
                return (int)$value;
            case '?f' :
                return (float)$value;
            case '?a' :
                return $this->escapeSimpleList($value);
            case '?#' :
                return is_array($value)
                    ? $this->escapeNameList($value)
                    : $this->escapeName($value);
            default :
                throw new RuntimeException('Unsupported operator: ' . $operator);
        }
    }

    function escapeSimple($value)
    {
        if (is_null($value)) {
            return 'NULL';
        }
        if (is_numeric($value)) {
            return $value;
        } elseif (is_string($value)) {
            return "'" . mysqli_real_escape_string($this->mysql, $value) . "'";
        } elseif (is_bool($value)) {
            return $value ? 1 : 0;
        }
        throw new RuntimeException('Failed cast for simple type, value: ' . print_r($value, true));
    }

    function escapeSimpleList($values)
    {
        if (!is_array($values)) {
            throw new RuntimeException("Failed cast for array: " . print_r($values, true));
        }
        if ($this->isKeyString($values)) {
            $pairs = [];
            foreach ($values as $name => $value) {
                $pairs[] = $this->escapeName($name) . ' = ' . $this->escapeSimple($value);
            }
            return implode(', ', $pairs);
        } else {
            foreach ($values as $key => $value) {
                $values[$key] = $this->escapeSimple($value);
            }
            return implode(', ', $values);
        }
    }

    function escapeName($name)
    {
        return '`' . mysqli_real_escape_string($this->mysql, $name) . '`';
    }

    function escapeNameList($names)
    {
        foreach ($names as $key => $name) {
            $names[$key] = $this->escapeName($name);
        }
        return implode(', ', $names);
    }

    function isKeyString(array $array)
    {
        foreach ($array as $key => $value) {
            return is_string($key);
        }
        return false;
    }

}


enum PartType
{
    case TEXT;
    case OPERATOR;
    case BLOCK;
}


class Part
{
    public PartType $type;
    public $value;

    public function __construct(PartType $type, $value)
    {
        $this->type = $type;
        $this->value = $value;
    }
}
